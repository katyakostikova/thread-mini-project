class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async getByUsername({ username }) {
    const usernameIfExists = await this._userRepository.getByUsername(username);

    if (!usernameIfExists) {
      return { username: '' };
    }

    return usernameIfExists;
  }

  async editUser(id, data) {
    const user = await this._userRepository.updateById(id, data);

    return user;
  }
}

export { User };
