class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  async setReaction(userId, { commentId, postId, isLike = true }) {
    const updateOrDelete = reaction => (reaction.isLike === isLike
      ? this._commentReactionRepository.deleteById(reaction.id)
      : this._commentReactionRepository.updateById(reaction.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId,
      postId
    );
    const prevReaction = reaction?.isLike;

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({ userId, commentId, postId, isLike });

    return Number.isInteger(result)
      ? {}
      : {
        prevReaction,
        reaction: await this._commentReactionRepository.getCommentReaction(userId, commentId, postId)
      };
  }

  deleteComment(id) {
    return this._commentRepository.deleteById(id);
  }

  updateComment(id, commentData) {
    return this._commentRepository.updateById(id, commentData);
  }
}
export { Comment };
