const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  USERNAME: '/username'
};

export { AuthApiPath };
