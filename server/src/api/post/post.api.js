import { PostsApiPath, SocketEvent } from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit(SocketEvent.NEW_POST, post); // notify all users that a new post was created
      return post;
    })
    .put(PostsApiPath.REACT, async req => {
      const { reaction, prevReaction } = await postService.setReaction(req.user.id, req.body);
      if (reaction?.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        if (reaction?.isLike === true) {
          req.io
            .to(reaction.post.userId)
            .emit(SocketEvent.LIKE, 'Your post was liked!');
        } if (reaction?.isLike === false) {
          req.io
            .to(reaction.post.userId)
            .emit(SocketEvent.DISLIKE, 'Your post was disliked!');
        }
      }
      return { reaction, prevReaction };
    })
    // update post
    .put(PostsApiPath.$ID, async req => {
      const updatedPost = await postService.updatePost({
        ...req.body.postData
      });
      return updatedPost;
    })
    .delete(PostsApiPath.$ID, async req => {
      const deletedPost = await postService.deletePost(req.params.id);
      return deletedPost;
    });

  done();
};

export { initPost };
