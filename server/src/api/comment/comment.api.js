import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (router, opts, done) => {
  const { comment: commentService } = opts.services;

  router
    .get(CommentsApiPath.$ID, req => commentService.getCommentById(req.params.id))
    .post(CommentsApiPath.ROOT, req => commentService.create(req.user.id, req.body))
    .put(CommentsApiPath.REACT, async req => {
      const { reaction, prevReaction } = await commentService.setReaction(req.user.id, req.body);
      return { reaction, prevReaction };
    })
    .delete(CommentsApiPath.$ID, req => {
      const deletedComment = commentService.deleteComment(req.params.id);
      return deletedComment;
    })
    .put(CommentsApiPath.$ID, req => {
      const updatedComment = commentService.updateComment(req.params.id, req.body);
      return updatedComment;
    });

  done();
};

export { initComment };
