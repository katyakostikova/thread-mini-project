const TableName = {
  COMMENT_REACTIONS: 'comment_reactions',
  COMMENTS: 'comments',
  USERS: 'users',
  POSTS: 'posts'
};

const ColumnName = {
  CREATED_AT: 'created_at',
  ID: 'id',
  IS_LIKE: 'is_like',
  UPDATED_AT: 'updated_at',
  USER_ID: 'user_id',
  POST_ID: 'post_id',
  COMMENT_ID: 'comment_id'
};

const RelationRule = {
  CASCADE: 'CASCADE',
  SET_NULL: 'SET NULL'
};

export async function up(knex) {
  await knex.schema.createTable(TableName.COMMENT_REACTIONS, table => {
    table.increments(ColumnName.ID).primary();
    table.boolean(ColumnName.IS_LIKE).notNullable().defaultTo(true);
    table
      .dateTime(ColumnName.CREATED_AT)
      .notNullable()
      .defaultTo(knex.fn.now());
    table
      .dateTime(ColumnName.UPDATED_AT)
      .notNullable()
      .defaultTo(knex.fn.now());
  });
  await knex.schema.alterTable(TableName.COMMENT_REACTIONS, table => {
    table
      .integer(ColumnName.USER_ID)
      .references(ColumnName.ID)
      .inTable(TableName.USERS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
    table
      .integer(ColumnName.COMMENT_ID)
      .references(ColumnName.ID)
      .inTable(TableName.COMMENTS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
    table
      .integer(ColumnName.POST_ID)
      .references(ColumnName.ID)
      .inTable(TableName.POSTS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
  });
}

export async function down(knex) {
  await knex.schema.dropTableIfExists(TableName.COMMENT_REACTIONS);
  await knex.schema.alterTable(TableName.COMMENT_REACTIONS, table => {
    table.dropColumn(ColumnName.USER_ID);
    table.dropColumn(ColumnName.POST_ID);
    table.dropColumn(ColumnName.COMMENT_ID);
  });
}
