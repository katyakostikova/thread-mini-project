const TableName = {
  USERS: 'users'
};

const ColumnName = {
  STATUS: 'status'
};

export async function up(knex) {
  await knex.schema.table(TableName.USERS, table => {
    table.string(ColumnName.STATUS).defaultTo(null);
  });
}

export async function down(knex) {
  await knex.schema.table(TableName.USERS, table => {
    table.dropColumn(ColumnName.STATUS);
  });
}
