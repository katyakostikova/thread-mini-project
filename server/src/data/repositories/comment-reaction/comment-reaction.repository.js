import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel }) {
    super(commentReactionModel);
  }

  getCommentReaction(userId, commentId, postId) {
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ commentId })
      .andWhere({ postId })
      .withGraphFetched('[comment]')
      .first();
  }
}

export { CommentReaction };
