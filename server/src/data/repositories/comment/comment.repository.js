import { Abstract } from '../abstract/abstract.repository';
import { getCommentReactionsQuery } from '../post/helpers';

class Comment extends Abstract {
  constructor({ commentModel }) {
    super(commentModel);
  }

  getCommentById(id) {
    return this.model
      .query()
      .select(
        'comments.*',
        getCommentReactionsQuery(this.model)(true),
        getCommentReactionsQuery(this.model)(false)
      )
      .findById(id)
      .withGraphFetched('[user.image]');
  }
}

export { Comment };
