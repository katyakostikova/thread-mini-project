const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions').count().where({ isLike }).as(col);
};

const getCommentReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model
    .relatedQuery('commentReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getWhereUserIdQuery = (userId, isHide) => builder => {
  if (userId && isHide) {
    builder.whereNot({ userId });
  } else if (userId) {
    builder.where({ userId });
  }
};

export {
  getCommentsCountQuery,
  getReactionsQuery,
  getWhereUserIdQuery,
  getCommentReactionsQuery
};
