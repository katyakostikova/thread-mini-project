import { Abstract } from '../abstract/abstract.repository';
import { CommentModel } from '../../models/index';

import {
  getCommentsCountQuery,
  getReactionsQuery,
  getCommentReactionsQuery,
  getWhereUserIdQuery
} from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const { from: offset, count: limit, userId, isLiked, isHide } = filter;

    if (isLiked) {
      return this.model
        .query()
        .select(
          'posts.*',
          getCommentsCountQuery(this.model),
          getReactionsQuery(this.model)(true),
          getReactionsQuery(this.model)(false)
        )
        .whereExists(
          this.model
            .relatedQuery('postReactions')
            .where('postReactions.userId', userId)
            .where('postReactions.isLike', isLiked)
        )
        .withGraphFetched('[image, user.image]')
        .orderBy('createdAt', 'desc')
        .offset(offset)
        .limit(limit);
    }

    return this.model
      .query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereUserIdQuery(userId, isHide))
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model
      .query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image, ]')
      .modifyGraph('comments', builder => {
        builder.select(
          'comments.*',
          getCommentReactionsQuery(CommentModel)(true),
          getCommentReactionsQuery(CommentModel)(false)
        );
      })
      .first();
  }
}

export { Post };
