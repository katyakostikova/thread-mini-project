const IconName = {
  CAT: 'cat',
  COMMENT: 'comment',
  ENVELOPE: 'envelope',
  EXCLAMATION_TRIANGLE: 'exclamation-triangle',
  HOME: 'home',
  INFO: 'info',
  LOCK: 'lock',
  PAPER_PLANE: 'paper-plane',
  PLUS_SQUARE: 'plus-square',
  SHARE_ALT: 'share-alt',
  THUMBS_UP: 'thumbs-up',
  THUMBS_DOWN: 'thumbs-down',
  USER: 'user',
  PEN: 'pen',
  BIN: 'trash-alt',
  CANCEL: 'times-circle',
  APPLY: 'check-circle',
  STATUS: 'comment-dots'
};

export { IconName };
