const HomeScreenName = {
  THREAD: 'Thread',
  EXPANDED_POST: 'Expanded Post',
  EDIT_POST: 'Edit Post'
};

export { HomeScreenName };
