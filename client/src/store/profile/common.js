const ActionType = {
  LOG_IN: 'profile/log-in',
  LOG_OUT: 'profile/log-out',
  REGISTER: 'profile/register',
  EDIT_USER: 'profile/edit-user'
};

export { ActionType };
