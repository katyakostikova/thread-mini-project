import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const updatePost = createAsyncThunk(
  ActionType.EDIT_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.editPost(post);
    const editedPost = await services.post.getPost(id);

    return { post: editedPost };
  }
);

const loadExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { prevReaction, reaction } = await services.post.likePost(postId);
    const diff = reaction ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const dislikeDif = prevReaction === false ? -1 : 0;

    const mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
      dislikeCount: Number(post.dislikeCount) + dislikeDif
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
    const updatedExpandedPost = expandedPost?.id === postId ? mapLikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { prevReaction, reaction } = await services.post.dislikePost(postId);
    const diff = reaction ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const likeDif = prevReaction ? -1 : 0;

    const mapDislikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
      likeCount: Number(post.likeCount) + likeDif // diff is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));
    const updatedExpandedPost = expandedPost?.id === postId ? mapDislikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { extra: { services } }) => {
    await services.post.deletePost(postId);
    return postId;
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === comment.postId ? mapComments(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const likeComment = createAsyncThunk(
  ActionType.COMMENT_REACT,
  async ({ commentId, postId }, { getState, extra: { services } }) => {
    const { prevReaction, reaction } = await services.comment.likeComment(commentId, postId);
    const diff = reaction ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const dislikeDif = prevReaction === false ? -1 : 0;

    const mapLikes = comments => ({
      ...comments,
      likeCount: Number(comments.likeCount) + diff, // diff is taken from the current closure
      dislikeCount: Number(comments.dislikeCount) + dislikeDif
    });

    const {
      posts: { expandedPost }
    } = getState();
    const { comments } = expandedPost;
    const updated = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

    return { comments: updated };
  }
);

const dislikeComment = createAsyncThunk(
  ActionType.COMMENT_REACT,
  async ({ commentId, postId }, { getState, extra: { services } }) => {
    const { prevReaction, reaction } = await services.comment.dislikeComment(commentId, postId);
    const diff = reaction ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const likeDif = prevReaction ? -1 : 0;

    const mapDislikes = comment => ({
      ...comment,
      dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
      likeCount: Number(comment.likeCount) + likeDif // diff is taken from the current closure
    });

    const {
      posts: { expandedPost }
    } = getState();
    const { comments } = expandedPost;
    const updated = comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

    return { comments: updated };
  }
);

const deleteComment = createAsyncThunk(
  ActionType.DELETE_COMMENT,
  async ({ commentId, postId }, { extra: { services } }) => {
    await services.comment.deleteComment(commentId);
    return { commentId, postId };
  }
);

const editComment = createAsyncThunk(
  ActionType.EDIT_COMMENT,
  async ({ commentId, body }, { extra: { services } }) => {
    const { id } = await services.comment.editComment(commentId, body);
    const editedComment = await services.comment.getComment(id);

    return { editedComment };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  loadExpandedPost,
  likePost,
  dislikePost,
  addComment,
  updatePost,
  deletePost,
  likeComment,
  dislikeComment,
  deleteComment,
  editComment
};
