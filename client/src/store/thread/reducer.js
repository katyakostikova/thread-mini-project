import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';
import * as profileActions from '../profile/actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(threadActions.updatePost.fulfilled, (state, action) => {
    const { post: updatedPost } = action.payload;
    const postToUpdate = state.posts.find(post => post.id === updatedPost.id);
    const { body, image, updatedAt } = updatedPost;
    postToUpdate.body = body;
    postToUpdate.image = image;
    postToUpdate.updatedAt = updatedAt;
  });
  builder.addCase(threadActions.deletePost.fulfilled, (state, action) => {
    const postId = action.payload;
    const deletePostIndex = state.posts.findIndex(post => post.id === postId);
    state.posts.splice(deletePostIndex, 1);
  });
  builder.addCase(threadActions.deleteComment.fulfilled, (state, action) => {
    const { commentId, postId } = action.payload;
    const deleteCommentId = state.expandedPost.comments.findIndex(comment => comment.id === commentId);
    state.expandedPost.commentCount -= 1;
    state.expandedPost.comments.splice(deleteCommentId, 1);
    const postCountId = state.posts.findIndex(post => post.id === postId);
    state.posts[postCountId].commentCount -= 1;
  });
  builder.addCase(threadActions.editComment.fulfilled, (state, action) => {
    const { editedComment } = action.payload;
    const editCommentId = state.expandedPost.comments.findIndex(comment => comment.id === editedComment.id);
    state.expandedPost.comments[editCommentId] = editedComment;
  });
  builder.addCase(profileActions.editUser.fulfilled, (state, action) => {
    const user = action.payload;

    state.posts.map(post => {
      if (post.id === user.id) {
        post.user = user;
      }
      return post;
    });

    if (state.expandedPost) {
      state.expandedPost.user = user;
      state.expandedPost.comments.map(comment => {
        if (comment.id === user.id) {
          comment.user = user;
        }
        return comment;
      });
    }
  });
  builder.addMatcher(isAnyOf(
    threadActions.likeComment.fulfilled,
    threadActions.dislikeComment.fulfilled
  ), (state, action) => {
    const { comments } = action.payload;
    state.expandedPost.comments = comments;
  });
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.fulfilled,
      threadActions.dislikePost.fulfilled,
      threadActions.addComment.fulfilled
    ),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.applyPost.fulfilled,
      threadActions.createPost.fulfilled
    ),
    (state, action) => {
      const { post } = action.payload;

      state.posts = [post, ...state.posts];
    }
  );
});

export { reducer };
