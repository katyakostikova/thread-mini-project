const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  COMMENT: 'thread/comment',
  EDIT_POST: 'thread/edit-post',
  DELETE_POST: 'thread/delete-post',
  COMMENT_REACT: 'thread/comment/react',
  DELETE_COMMENT: 'thread/delete-comment',
  EDIT_COMMENT: 'thread/edit-comment'
};

export { ActionType };

