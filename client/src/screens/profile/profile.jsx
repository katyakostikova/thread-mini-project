import * as React from 'react';
import {
  IconName,
  NotificationMessage,
  TextVariant,
  UserPayloadKey,
  UserValidationMessage
} from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { Button, Image, Input, Stack, Text, View } from 'components/components';
import { useAppForm, useDispatch, useSelector, useState } from 'hooks/hooks';
import { profileEdit as profileEditValidationSchema } from 'validation-schemas/validation-schemas';
import { profileActionCreator } from 'store/actions';
import { checkUniqueUsername, pickImage } from 'helpers/helpers';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import { ScrollView } from 'react-native';
import styles from './styles';

const Profile = () => {
  const dispatch = useDispatch();
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const { control, errors, handleSubmit, setValue } = useAppForm({
    defaultValues: {
      [UserPayloadKey.USERNAME]: user?.username,
      [UserPayloadKey.EMAIL]: user?.email,
      [UserPayloadKey.STATUS]: user?.status ? user?.status : ''
    },
    validationSchema: profileEditValidationSchema
  });
  const [isEdit, setIsEdit] = useState(false);
  const [isError, setIsError] = useState(false);
  const [image, setImage] = useState(user?.image);
  const [isUploading, setIsUploading] = useState(false);
  const [restartApp, setRestartApp] = useState(false);

  const handleUploadFile = async () => {
    setIsUploading(true);

    try {
      const data = await pickImage();
      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  const handleUserLogout = () => dispatch(profileActionCreator.logout());

  const changeViewToEdit = () => {
    setIsEdit(true);
    setRestartApp(false);
  };

  const handleCancelUpdate = () => {
    setIsEdit(false);
    setIsError(false);
    setRestartApp(false);
    setValue(UserPayloadKey.USERNAME, user?.username);
    setValue(UserPayloadKey.STATUS, user?.status ? user?.status : '');
  };

  const handleDeleteImage = () => {
    setImage(undefined);
  };

  const errorUsernameIsTaken = (
    <Text style={styles.errorMessage}>
      {UserValidationMessage.USERNAME_UNIQUE}
    </Text>
  );

  const handleProfileEdit = async values => {
    setIsError(false);
    const username = await checkUniqueUsername({ username: values.username });
    if (username && username !== user?.username) {
      setIsError(true);
      return;
    }
    dispatch(
      profileActionCreator.editUser({
        username: values.username,
        status: values.status,
        imageId: image?.id ? image?.id : null
      })
    );
    setIsEdit(false);
    setRestartApp(true);
  };

  if (!user) {
    return <></>;
  }

  return (
    <View style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
        <View style={styles.content}>
          <Image
            style={styles.avatar}
            accessibilityIgnoresInvertColors
            source={{ uri: image?.link ?? DEFAULT_USER_AVATAR }}
          />
          {isEdit && (
            <View style={styles.imageButtonContainer}>
              <Button title="Delete avatar" onPress={handleDeleteImage} />
              <Button title="Change avatar" onPress={handleUploadFile} />
            </View>
          )}
          <Stack space={15}>
            <Input
              name={UserPayloadKey.USERNAME}
              control={control}
              errors={errors}
              iconName={IconName.USER}
              isDisabled={!isEdit}
            />
            <Input
              name={UserPayloadKey.EMAIL}
              control={control}
              errors={errors}
              iconName={IconName.ENVELOPE}
              isDisabled
            />
            <Input
              name={UserPayloadKey.STATUS}
              control={control}
              errors={errors}
              iconName={IconName.STATUS}
              placeholder="Create your status"
              isDisabled={!isEdit}
            />
            {isError && errorUsernameIsTaken}
            <View style={styles.buttonContainer}>
              {isEdit ? (
                <>
                  <Button title="Cancel" onPress={handleCancelUpdate} />
                  <Button
                    title="Edit"
                    isDisabled={isUploading}
                    onPress={handleSubmit(handleProfileEdit)}
                  />
                </>
              ) : (
                <>
                  <Button title="Logout" onPress={handleUserLogout} />
                  <Button title="Edit profile" onPress={changeViewToEdit} />
                </>
              )}
            </View>
            {restartApp && (
              <Text variant={TextVariant.TEXT}>
                Please restart the app to apply changes
              </Text>
            )}
          </Stack>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profile;
