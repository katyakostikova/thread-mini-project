import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
    padding: 15,
    paddingTop: 60
  },
  content: {
    minWidth: 240
  },
  avatar: {
    marginBottom: 30,
    width: 250,
    height: 250,
    borderRadius: 125
  },
  buttonContainer: {
    height: 100,
    width: '40%',
    justifyContent: 'space-between',
    alignSelf: 'center'
  },
  imageButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
    alignItems: 'center'
  }
});

export default styles;
