import * as React from 'react';
import { IconName, TextVariant } from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import PropTypes from 'prop-types';
import { commentType } from 'common/prop-types/prop-types';
import { Icon, Image, Stack, Text, View } from 'components/components';
import { getFromNowTime } from 'helpers/helpers';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import { TextInput } from 'react-native';
import styles from './styles';

const Comment = ({
  comment: { id, body, createdAt, user, postId, likeCount, dislikeCount },
  onCommentLike,
  onCommentDislike,
  onCommentDelete,
  onCommentEdit
}) => {
  const [isEdit, setIsEdit] = useState(false);
  const [commentBody, setCommentBody] = useState(body);
  const handleCommentLike = () => onCommentLike({ commentId: id, postId });
  const handleCommentDislike = () => onCommentDislike({ commentId: id, postId });
  const handleCommentDelete = () => onCommentDelete({ commentId: id, postId });

  const enableCommentEdit = () => {
    setIsEdit(true);
    setCommentBody(body);
  };

  const handleCommentEdit = () => {
    if (!commentBody) {
      return;
    }
    setIsEdit(false);
    onCommentEdit({ commentId: id, body: commentBody });
  };

  const currentUserId = useSelector(state => state.profile.user?.id);

  return (
    <View style={styles.container}>
      <View style={styles.commentContent}>
        <Image
          style={styles.avatar}
          accessibilityIgnoresInvertColors
          source={{ uri: user.image?.link ?? DEFAULT_USER_AVATAR }}
        />
        <View style={styles.content}>
          <View style={styles.header}>
            <View>
              <Text variant={TextVariant.TITLE}>{user.username}</Text>
            </View>
            {!isEdit && (
              <Text variant={TextVariant.SUBTITLE}>
                {' '}
                •
                {getFromNowTime(createdAt)}
              </Text>
            )}
          </View>
          {isEdit && (
            <View style={styles.editCommentContainer}>
              <TextInput
                multiline
                value={commentBody}
                numberOfLines={3}
                onChangeText={setCommentBody}
                style={styles.input}
              />
              <Icon
                name={IconName.CANCEL}
                size={25}
                onPress={() => setIsEdit(false)}
              />
              <Icon
                name={IconName.APPLY}
                size={25}
                onPress={handleCommentEdit}
              />
            </View>
          )}
          {!isEdit && (
            <>
              {user?.status && (
                <View style={styles.statusContainer}>
                  <Text variant={TextVariant.SUBTITLE}>{user?.status}</Text>
                </View>
              )}
              <Text style={styles.body}>{body}</Text>
            </>
          )}
        </View>
      </View>
      {!isEdit && (
        <View style={styles.footer}>
          <Stack space={24} isRow>
            <Icon
              name={IconName.THUMBS_UP}
              size={16}
              label={String(likeCount)}
              onPress={handleCommentLike}
            />
            <Icon
              name={IconName.THUMBS_DOWN}
              size={16}
              label={String(dislikeCount)}
              onPress={handleCommentDislike}
            />
          </Stack>
          <View style={styles.footerRightButtons}>
            {currentUserId === user.id ? (
              <>
                <Icon
                  style={styles.rightButton}
                  name={IconName.PEN}
                  size={16}
                  onPress={enableCommentEdit}
                />
                <Icon
                  style={styles.rightButton}
                  name={IconName.BIN}
                  size={16}
                  onPress={handleCommentDelete}
                />
              </>
            ) : (
              <></>
            )}
          </View>
        </View>
      )}
    </View>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired,
  onCommentEdit: PropTypes.func.isRequired
};

export default Comment;
