import { StyleSheet } from 'react-native';
import { AppColor } from 'config/config';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: AppColor.BORDER
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 25
  },
  content: {
    flex: 1,
    marginLeft: 10
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  body: {
    fontSize: 14
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 50,
    marginVertical: 5
  },
  commentContent: {
    flexDirection: 'row'
  },
  footerRightButtons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  rightButton: {
    paddingRight: 10
  },
  editCommentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  input: {
    height: 50,
    width: '70%',
    fontSize: 14,
    padding: 3,
    marginVertical: 5,
    color: AppColor.TEXT,
    backgroundColor: AppColor.INPUT,
    textAlignVertical: 'top',
    borderRadius: 5
  },
  statusContainer: {
    marginBottom: 5
  }
});

export default styles;
