import * as React from 'react';
import { NotificationMessage } from 'common/enums/enums';
import { Post, FlatList, Spinner } from 'components/components';
import { sharePost } from 'helpers/helpers';
import {
  useCallback,
  useDispatch,
  useNavigation,
  useSelector
} from 'hooks/hooks';
import { notification as notificationService } from 'services/services';
import { threadActionCreator } from 'store/actions';
import { AddComment, Comment } from './components/components';
import { getSortedComments } from './helpers/helpers';
import styles from './styles';

const ExpandedPost = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const sortedComments = getSortedComments(post?.comments ?? []);

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleCommentLike = useCallback(
    id => dispatch(threadActionCreator.likeComment(id)),
    [dispatch]
  );

  const handleCommentDislike = useCallback(
    id => dispatch(threadActionCreator.dislikeComment(id)),
    [dispatch]
  );

  const handleCommentAdd = useCallback(
    commentPayload => dispatch(threadActionCreator.addComment(commentPayload)),
    [dispatch]
  );

  const handleCommentEdit = useCallback(
    commentPayload => dispatch(threadActionCreator.editComment(commentPayload)),
    [dispatch]
  );

  const handleCommentDelete = useCallback(
    id => {
      dispatch(threadActionCreator.deleteComment(id));
    },
    [dispatch]
  );

  const handlePostDelete = useCallback(
    id => {
      dispatch(threadActionCreator.deletePost(id));
      navigation.pop();
    },
    [dispatch]
  );

  const handlePostShare = useCallback(
    ({ body, image }) => sharePost({ body, image }).catch(() => {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    }),
    []
  );

  if (!post) {
    return <Spinner isOverflow />;
  }

  return (
    <FlatList
      bounces={false}
      data={sortedComments}
      keyExtractor={({ id }) => id}
      ListHeaderComponentStyle={styles.header}
      contentContainerStyle={styles.container}
      ListHeaderComponent={(
        <>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostShare={handlePostShare}
            onPostDelete={handlePostDelete}
          />
          <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
        </>
      )}
      renderItem={({ item: comment }) => (
        <Comment
          comment={comment}
          onCommentLike={handleCommentLike}
          onCommentDislike={handleCommentDislike}
          onCommentDelete={handleCommentDelete}
          onCommentEdit={handleCommentEdit}
        />
      )}
    />
  );
};

export default ExpandedPost;
