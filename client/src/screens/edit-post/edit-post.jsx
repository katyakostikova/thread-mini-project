import * as React from 'react';
import { TextInput } from 'react-native';
import {
  ButtonVariant,
  HomeScreenName,
  IconName,
  NotificationMessage,
  TextVariant
} from 'common/enums/enums';
import { Button, Image, Text, View } from 'components/components';
import { AppColor } from 'config/config';
import { pickImage } from 'helpers/helpers';
import { postType } from 'common/prop-types/prop-types';
import PropTypes from 'prop-types';
import { useDispatch, useNavigation, useState } from 'hooks/hooks';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import { threadActionCreator } from 'store/actions';
import styles from './styles';

const EditPost = ({ route }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const {
    id: postId,
    image: postImage,
    body: postBody
  } = route.params.post;

  const [body, setBody] = useState(postBody);
  const [image, setImage] = useState(postImage);
  const [isUploading, setIsUploading] = useState(false);

  const handleEditPost = () => {
    if (!body) {
      return;
    }

    const updatedPost = {
      id: postId,
      imageId: image?.id,
      body
    };
    dispatch(threadActionCreator.updatePost(updatedPost));
    setBody('');
    setImage(undefined);
    navigation.navigate(HomeScreenName.THREAD);
  };

  const handleUploadFile = async () => {
    setIsUploading(true);

    try {
      const data = await pickImage();
      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <View style={styles.screen}>
      <Text variant={TextVariant.HEADLINE}>Edit Post</Text>
      <TextInput
        multiline
        value={body}
        placeholder="Type something here..."
        placeholderTextColor={AppColor.PLACEHOLDER}
        numberOfLines={10}
        style={styles.input}
        onChangeText={setBody}
      />
      <View style={styles.buttonWrapper}>
        <Button
          title="Attach image"
          variant={ButtonVariant.TEXT}
          icon={IconName.PLUS_SQUARE}
          isLoading={isUploading}
          onPress={handleUploadFile}
        />
      </View>
      {image?.link && (
        <Image
          style={styles.image}
          accessibilityIgnoresInvertColors
          source={{ uri: image?.link }}
        />
      )}
      <Button
        title="Edit post"
        isDisabled={!body || isUploading}
        onPress={handleEditPost}
      />
    </View>
  );
};

EditPost.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      post: postType.isRequired
    }).isRequired
  }).isRequired
};

export default EditPost;
