import { HttpMethod, ContentType } from '../../common/enums/enums';

class Comment {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load(`${this._apiPath}/comments`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId, postId) {
    return this._http.load(`${this._apiPath}/comments/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        postId,
        isLike: true
      })
    });
  }

  dislikeComment(commentId, postId) {
    return this._http.load(`${this._apiPath}/comments/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        postId,
        isLike: false
      })
    });
  }

  deleteComment(commentId) {
    return this._http.load(`${this._apiPath}/comments/${commentId}`, {
      method: HttpMethod.DELETE
    });
  }

  editComment(commentId, body) {
    return this._http.load(`${this._apiPath}/comments/${commentId}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        body
      })
    });
  }
}

export { Comment };
