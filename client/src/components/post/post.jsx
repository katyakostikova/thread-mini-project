import * as React from 'react';
import PropTypes from 'prop-types';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { HomeScreenName, IconName, TextVariant } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { Icon, Image, Stack, Text, View } from 'components/components';
import { getFromNowTime } from 'helpers/helpers';
import { useSelector } from 'react-redux';
import { useNavigation } from 'hooks/hooks';
import { useCallback } from 'react';
import styles from './styles';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onPostShare,
  onPostExpand,
  onPostDelete
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const date = getFromNowTime(createdAt);
  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostExpand = () => onPostExpand(id);
  const handlePostShare = () => onPostShare({ body, image });
  const handlePostDelete = () => onPostDelete(id);
  const currentUserId = useSelector(state => state.profile.user?.id);

  const navigation = useNavigation();
  const handlePostEdit = useCallback(() => {
    navigation.navigate(HomeScreenName.EDIT_POST, { post });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        accessibilityIgnoresInvertColors
        source={{ uri: user?.image?.link ?? DEFAULT_USER_AVATAR }}
      />
      <View style={styles.content}>
        <View style={styles.header}>
          <Text variant={TextVariant.TITLE}>{user.username}</Text>
          <Text variant={TextVariant.SUBTITLE}>
            {' • '}
            {date}
          </Text>
        </View>
        {image && (
          <Image
            style={styles.image}
            accessibilityIgnoresInvertColors
            source={{ uri: image.link }}
          />
        )}
        <Text style={styles.body}>{body}</Text>
        <View style={styles.footer}>
          <Stack space={24} isRow>
            <Icon
              name={IconName.THUMBS_UP}
              size={16}
              label={String(likeCount)}
              onPress={handlePostLike}
            />
            <Icon
              name={IconName.THUMBS_DOWN}
              size={16}
              label={String(dislikeCount)}
              onPress={handlePostDislike}
            />
            <Icon
              name={IconName.COMMENT}
              size={16}
              label={String(commentCount)}
              onPress={onPostExpand ? handlePostExpand : null}
            />
          </Stack>
          <View style={styles.footerRightButtons}>
            {currentUserId === user?.id ? (
              <>
                <Icon
                  style={styles.rightButton}
                  name={IconName.PEN}
                  size={16}
                  onPress={handlePostEdit}
                />
                <Icon
                  style={styles.rightButton}
                  name={IconName.BIN}
                  size={16}
                  onPress={handlePostDelete}
                />
              </>
            ) : (
              <></>
            )}
            <Icon
              name={IconName.SHARE_ALT}
              size={16}
              onPress={handlePostShare}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostShare: PropTypes.func.isRequired,
  onPostExpand: PropTypes.func,
  onPostDislike: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired
};

Post.defaultProps = {
  onPostExpand: null
};

export default Post;
