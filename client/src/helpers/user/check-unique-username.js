import { auth as authService } from 'services/services';

const checkUniqueUsername = async usernameToCheck => {
  const { username } = await authService.getUsername(usernameToCheck);
  return username;
};

export { checkUniqueUsername };
